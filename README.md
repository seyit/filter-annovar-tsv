# filter-annovar-tsv

Filter .annotated.genome.tsv from wAnnovar annotation tool with given field. Calculate variant fraction. Save as new file.

## Usage

python filter_annovar.py --infile <INPUT_FILE_PATH> --outfile <OUTPUT_FILE_PATH> [ARGUMENTS]

**ARGS**
- infile : input file path, format: wannovar annotated.genome.tsv
- outfile : output file, tsv
- filter_hom : get all homozygous variants
- filter_het : get all heterozygous variants
- all : get both het and hom variants
- exonic : get all exonic variants
- frameshift : get all frameshift variants
- nonsynonymous : get all nonsynonymous variants
- freq_threshold : get variants with population freq lower then threshold, float
- dp_threshold : filter out any variant with depth lower then threshold, integer

_**ex:** -het -exonic -nonsynonymous -frameshift -freq 0.01 -dp 5_

## Disclosure
This script was written for a one-time need as part of an academic project in 2019. Not suitable for all needs. The current wAnnovar output may fail. Before using, you may need to make changes considering these conditions.
