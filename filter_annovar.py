import argparse

threshold_het = 0.3
threshold_hom = 0.6

KEYS_1000G = ['1000G_ALL', '1000G_AFR', '1000G_AMR', '1000G_EAS', '1000G_EUR', '1000G_SAS']
KEYS_Exac = ['ExAC_Freq', 'ExAC_AFR', 'ExAC_AMR', 'ExAC_EAS', 'ExAC_FIN', 'ExAC_NFE', 'ExAC_OTH', 'ExAC_SAS']
KEYS_ESP6500 = ['ESP6500si_ALL', 'ESP6500si_AA', 'ESP6500si_EA']
KEYS_Gnomad = ['gnomAD_exome_ALL', 'gnomAD_exome_AFR', 'gnomAD_exome_AMR', 'gnomAD_exome_ASJ', 'gnomAD_exome_EAS',
               'gnomAD_exome_FIN', 'gnomAD_exome_NFE', 'gnomAD_exome_OTH', 'gnomAD_exome_SAS', 'gnomAD_genome_ALL',
               'gnomAD_genome_AFR', 'gnomAD_genome_AMR', 'gnomAD_genome_ASJ', 'gnomAD_genome_EAS', 'gnomAD_genome_FIN',
               'gnomAD_genome_NFE', 'gnomAD_genome_OTH']


def float_freqs(raw_freqs):
    freqs = []
    for f in raw_freqs:
        try:
            freq = float(f)
        except ValueError:
            freq = f
        freqs.append(freq)
    return freqs

def collect_pop_freq(variant):
    pop_freq = {
        "1000g": float_freqs([variant[key] for key in KEYS_1000G]),
        "Exac": float_freqs([variant[key] for key in KEYS_Exac]),
        "ESP6500": float_freqs([variant[key] for key in KEYS_ESP6500]),
        "Gnomad": float_freqs([variant[key] for key in KEYS_Gnomad])
    }
    return pop_freq

def calc_fraction(sar, saf, srr, srf):
    if any([len(sar.split(',')) > 1, len(saf.split(',')) > 1]):
        return -1.0
    else:
        return round((int(sar) + int(saf)) / (int(sar) + int(saf) + int(srr) + int(srf)), 2)


def calc_zygosity(fraction, threshold_het, threshold_hom):
    if fraction < float(threshold_het):
        return "-"
    elif float(threshold_het) <= fraction <= float(threshold_hom):
        return "HET"
    elif fraction > float(threshold_hom):
        return "HOM"


def filter_freq(pop_freqs, freq_threshold):
    for g, fs in pop_freqs.items():
        floats = [f for f in fs if type(f) == float]
        all_good = all([i < freq_threshold for i in floats])
        if not all_good:
            return False
    return True


def run(infile, outfile,
        filter_hom, filter_het, filter_all, filter_exonic, filter_nonsynonymous,
        filter_frameshift, freq_threshold, dp_threshold):
    raw_count = 0
    filtered_count = 0
    with open(infile, 'r') as fi:
        with open(outfile, 'w') as fo:
            lines = fi.readlines()
            header, variants = lines[0].strip('\n').split('\t'), lines[1:]
            header.append('Fraction\tHOM\n')
            fo.write('\t'.join(header))
            for v in variants:
                raw_count += 1
                variant = v.strip('\n').split('\t')
                variant_dict = dict(zip(header, variant))
                if freq_threshold:
                    freq_threshold = float(freq_threshold)
                    pop_freqs = collect_pop_freq(variant_dict)
                    if not filter_freq(pop_freqs, freq_threshold):
                        continue

                if filter_exonic:
                    if "exonic" not in variant_dict['Func.refGene']:
                        continue

                if filter_exonic:
                    if "ncRNA" in variant_dict['Func.refGene']:
                        continue

                if filter_frameshift or filter_nonsynonymous:
                    filter_exonic_func = any([
                        (filter_frameshift and variant_dict['ExonicFunc.refGene'].startswith("frameshift")),
                        (filter_nonsynonymous and "nonsynonymous" in variant_dict['ExonicFunc.refGene'])
                    ])

                    if not filter_exonic_func:
                        continue

                info = variant[-3].split(';')
                info_dict = {i.split('=')[0]: i.split('=')[1] for i in info}
                if dp_threshold:
                    if float(info_dict['DP']) < float(dp_threshold):
                        continue
                fraction = calc_fraction(info_dict['SAR'], info_dict['SAF'], info_dict['SRR'], info_dict['SRF'])
                zygosity = calc_zygosity(fraction, threshold_het, threshold_hom)
                if filter_all:
                    fo.write(v.strip('\n') + f"\t{fraction}\t{zygosity}\n")
                    filtered_count += 1
                else:
                    if filter_hom and zygosity.lower() == 'hom':
                        fo.write(v.strip('\n') + f"\t{fraction}\t{zygosity}\n")
                        filtered_count += 1
                    if filter_het and zygosity.lower() == 'het':
                        fo.write(v.strip('\n') + f"\t{fraction}\t{zygosity}\n")
                        filtered_count += 1

    return raw_count, filtered_count


if __name__ == '__main__':
    parser = argparse.ArgumentParser('filter_annovar')
    parser.add_argument('--infile', '-i', dest='infile')
    parser.add_argument('--outfile', '-o', dest='outfile')
    parser.add_argument('--filter_hom', '-hom', dest='filter_hom', action='store_true')
    parser.add_argument('--filter_het', '-het', dest='filter_het', action='store_true')
    parser.add_argument('--all', '-all', dest='filter_all', action='store_true')
    parser.add_argument('--exonic', '-exonic', dest='filter_exonic', action='store_true')
    parser.add_argument('--frameshift', '-frameshift', dest='filter_frameshift', action='store_true')
    parser.add_argument('--nonsynonymous', '-nonsynonymous', dest='filter_nonsynonymous', action='store_true')
    parser.add_argument('--freq_threshold', '-freq', dest='freq_threshold')
    parser.add_argument('--dp_threshold', '-dp', dest='dp_threshold')
    parser.set_defaults(feature=False)
    args = parser.parse_args()

    print("DONE", args)

    raw_count, filtered_count = run(args.infile, args.outfile,
                                    filter_all=args.filter_all, filter_het=args.filter_het, filter_hom=args.filter_hom,
                                    filter_exonic=args.filter_exonic, filter_nonsynonymous=args.filter_nonsynonymous,
                                    filter_frameshift=args.filter_frameshift,
                                    freq_threshold=args.freq_threshold, dp_threshold=args.dp_threshold)

    print("DONE", f"{filtered_count} variants edited and filtered from {raw_count}")
